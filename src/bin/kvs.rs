use clap::{App, Arg, SubCommand};

fn main() {
    let root_matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("A simple K/V store")
        .subcommand(
            SubCommand::with_name("get")
                .about("retrieve a value by key")
                .arg(Arg::with_name("KEY").required(true).index(1)),
        )
        .subcommand(
            SubCommand::with_name("set")
                .about("set a key by value")
                .arg(Arg::with_name("KEY").required(true).index(1))
                .arg(Arg::with_name("VALUE").required(true).index(2)),
        )
        .subcommand(
            SubCommand::with_name("rm")
                .about("remove a key by value")
                .arg(Arg::with_name("KEY").required(true).index(1)),
        )
        .get_matches();

    let (subcommand, matches) = root_matches.subcommand();

    let _matches = match matches {
        Some(matches) => matches,
        None => {
            eprintln!("error: no subcommand given, try using --help");
            std::process::exit(1);
        }
    };

    match subcommand {
        "get" => {
            // let key = matches.value_of("KEY").unwrap();

            panic!("unimplemented")
        }
        "set" => {
            // let key = matches.value_of("KEY").unwrap();
            // let value = matches.value_of("VALUE").unwrap();

            panic!("unimplemented")
        }
        "rm" => {
            // let key = matches.value_of("KEY").unwrap();

            panic!("unimplemented")
        }
        _ => unreachable!("unhandled subcommmand"),
    }
}
