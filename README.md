# kvs

This repository hosts the code I create as I work through PingCAP's [Practical Networked Applications in Rust](https://github.com/pingcap/talent-plan/tree/master/rust) course.
